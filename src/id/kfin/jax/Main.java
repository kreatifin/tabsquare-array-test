package id.kfin.jax;

import java.util.Arrays;
import java.util.Collections;

public class Main {


    public static void main(String[] args) {
        String theChars = "ABBGGZHI";
        String[] chars = theChars.split("");
        Arrays.sort(chars, Collections.reverseOrder());

        chars = Arrays.stream(chars).distinct().toArray(String[]::new);
        Arrays.stream(chars).forEach(System.out::print);
    }
}
